const log = require("debug")("shaka-waka");
const error = require("debug")("shaka-waka:error");

const shakaPackagerLog = require("debug")("shaka-packager");
const shakaPackagerError = require("debug")("shaka-packager:error");

const ffmpegLog = require("debug")("ffmpeg");
const ffmpegError = require("debug")("ffmpeg:error");

const gstreamerLog = require("debug")("gstreamer");
const gstreamerError = require("debug")("gstreamer:error");

const Koa = require("koa");
const serve = require("koa-static");
const route = require("koa-route");

const util = require("util");
const fs = require("fs");
const path = require("path");
const cp = require("child_process");

const rimraf = util.promisify(require("rimraf"));
const mkdir = util.promisify(fs.mkdir);

const readFile = util.promisify(fs.readFile);
const exec = cp.exec;

const chokidar = require("chokidar");

const config = {
  http: {
    port: 5000
  }
};

const state = {
  mpdAvailable: false,
  streamingChild: null,
  packagerChild: null,
};

/**
 * Cleans the media folder.
 * @returns {Promise<Child_Process, Error>}
 */
function clean() {
  log("Cleaning 'media' folder");
  return rimraf(path.resolve(__dirname, "media"))
    .then(() => {
      return mkdir(path.resolve(__dirname, "media"));
    });
}

/**
 * Starts an FFMPEG child process that grabs the X11 desktop.
 * @returns {Promise<Child_Process, Error>}
 */
function startDesktopGrabber() {
  ffmpegLog("Starting desktop grabber");
  const child = state.streamingChild = exec(`ffmpeg\
    -fflags +genpts\
    -video_size 1920x1080\
    -framerate 25\
    -f x11grab\
    -i :0.0+0,420\
    -preset ultrafast\
    -c:v libx264 -x264opts fps=25\
    -tune zerolatency\
    -g 50\
    -keyint_min 25\
    -b 900k\
    -framerate 25\
    -metadata service_provider="ROJO 2"\
    -metadata service_name="Channel 2"\
    -f mpegts\
    udp://127.0.0.1:9000`);
  child.stdout.on("data", (chunk) => ffmpegLog(chunk.toString("utf-8")));
  child.stderr.on("data", (chunk) => ffmpegError(chunk.toString("utf-8")));
  child.on("error", (err) => ffmpegError(err));
  child.on("exit", (code, signal) => {
    ffmpegLog("desktop grabber exit", code, signal);
    throw new Error("FFmpeg exit");
  });
  return Promise.resolve(child);
}

/**
 * Starts GStreamer.
 * @returns {Promise<Child_Process, Error>}
 */
function startGStreamerVideoTest() {
  gstreamerLog("Starting gstreamer");
  const child = state.streamingChild = exec(`gst-launch-1.0 -v\
    videotestsrc is-live=true\
    ! x264enc tune=zerolatency speed-preset=ultrafast bitrate=900\
    ! mpegtsmux\
    ! udpsink host=127.0.0.1 port=9000`);
  child.stdout.on("data", (chunk) => gstreamerLog(chunk.toString("utf-8")));
  child.stderr.on("data", (chunk) => gstreamerError(chunk.toString("utf-8")));
  child.on("error", (err) => gstreamerError(err));
  child.on("exit", (code, signal) => {
    gstreamerLog("gstreamer exit", code, signal);
    throw new Error("Gstreamer exit");
  });
  return Promise.resolve(child);

}

/**
 * Starts GStreamer.
 * @returns {Promise<Child_Process, Error>}
 */
function startGStreamer() {
  gstreamerLog("Starting gstreamer");
  const child = state.streamingChild = exec(`gst-launch-1.0 -v\
    v4l2src\
    ! timeoverlay\
    ! x264enc tune=zerolatency speed-preset=ultrafast bitrate=900\
    ! mpegtsmux\
    ! udpsink host=127.0.0.1 port=9000`);
  child.stdout.on("data", (chunk) => gstreamerLog(chunk.toString("utf-8")));
  child.stderr.on("data", (chunk) => gstreamerError(chunk.toString("utf-8")));
  child.on("error", (err) => gstreamerError(err));
  child.on("exit", (code, signal) => {
    gstreamerLog("gstreamer exit", code, signal);
    throw new Error("Gstreamer exit");
  });
  return Promise.resolve(child);

}

/**
 * Starts a shaka packager child process that grabs the X11 desktop.
 * @returns {Promise<Child_Process, Error>}
 */
function startShakaPackager() {
  return new Promise((resolve, reject) => {
    log("Starting shaka packager");
    const child = state.packagerChild = exec(`./packager 'in=udp://127.0.0.1:9000?reuse=1&interface=0.0.0.0,stream=video,init_segment=media/live.mp4,segment_template=media/live_$Time$.m4s,language=eng'\
      --segment_duration 10\
      --fragment_duration 1\
      --time_shift_buffer_depth 30\
      --min_buffer_time 10\
      --suggested_presentation_delay 2\
      --default_language eng\
      --minimum_update_period 5\
      --mpd_output media/live.mpd`);
    child.stdout.on("data", (chunk) => shakaPackagerLog(chunk.toString("utf-8")));
    child.stderr.on("data", (chunk) => shakaPackagerError(chunk.toString("utf-8")));
    child.on("error", (err) => shakaPackagerError(err));
    child.on("exit", (code, signal) => {
      shakaPackagerLog("shaka packager exit", code, signal);
      throw new Error("Shaka packager exit");
    });

    setTimeout(() => {
      return resolve(child);
    }, 1000);
  });
}

/**
 * Starts an http server listening on 5000.
 * @returns {Promise<Koa, Error>}
 */
function startHttpServer() {
  log(`Starting http server on port ${config.http.port}`);
  const app = new Koa();

  app.use(serve(path.resolve(__dirname, "media")));
  app.use(route.get("/", async (ctx) => {
    ctx.set("Content-Type", "text/html");
    if (state.mpdAvailable) {
      const body = await readFile("index.html", { encoding: "utf-8" });
      ctx.body = body.replace(/{{port}}/, config.http.port);
    } else {
      const body = await readFile("not-ready.html", { encoding: "utf-8" });
      ctx.body = body.replace(/{{port}}/, config.http.port);
    }
  }));

  app.listen(config.http.port);
  return Promise.resolve(app);
}

/**
 * Starts the watcher for .mpd.
 * @returns {Promise<Watcher, Error>}
 */
function startWatcher() {
  log("Starting MPD watcher");
  const watcher = chokidar.watch("media/*.mpd");
  watcher.on("add", (path) => {
    log(`${path} created`);
    state.mpdAvailable = true;
  });
  watcher.on("change", (path) => {
    log(`${path} modified`);
  });
  return Promise.resolve(watcher);
}

clean()
  .then(startWatcher)
  .then(startShakaPackager)
  .then(startGStreamer)
  .then(startHttpServer);

process.on("uncaughtException", () => {
  if (state.packagerChild) {
    state.packagerChild.kill();
  }
  if (state.streamingChild) {
    state.streamingChild.kill();
  }
  process.exit(1);
});
