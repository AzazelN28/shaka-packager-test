# Live streaming services

## Problems and solutions

### How we can generate DASH compatible chunks?

To answer this question properly, we should differentiate the main two
aspects of generating a chunk: codecs and containers.

For ISO BMFF (MP4 container), we have two codecs: h.264 and h.265, the first
is more compatible between browsers.

For WebM (Matroska container), we have also two codecs: vp8 and vp9, being the first more compatible between browsers.

Almost every tested tool supports these containers and codecs, but with
some implementation details.

As I've seen, we can use shaka-packager, ffmpeg, gstreamer, bento MP4 set of tools or GPAC's MP4Box to create this chunks. There are several differences between them, for example, ffmpeg seems more capable of generating ISO BMFF chunks than GStreamer, bento MP4 and GPAC's MP4Box only
work for static MP4 files so they are more difficult to implement with
live streaming services.

Available tools:
- shaka-packager
- GPAC's MP4Box
- Bento4
- FFMPEG
- GStreamer

### How we can generate MPD?

Available tools:
- shaka-packager
- GPAC's MP4Box
- Bento4
- FFMPEG (seems that only works for WebM files)

### How can we

             -> Ingest VSL, MD, FD
IDC -> Chunk -> DASH Chunk

### Prepare for ingestion

