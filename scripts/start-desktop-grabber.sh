#!/bin/bash
ffmpeg -video_size 1920x1080 -r 25 -f x11grab -i :0.0+0,420 -preset ultrafast -vcodec libx264 -tune zerolatency -g 50 -b 900k -r 25 -f mpegts udp://127.0.0.1:9000
