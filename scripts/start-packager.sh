#!/bin/bash
./packager 'in=udp://127.0.0.1:9000?reuse=1&interface=0.0.0.0,stream=video,init_segment=media/live.mp4,segment_template=media/live_$Time$.m4s'\
  --segment_duration 60\
  --fragment_duration 10\
  --time_shift_buffer_depth 60\
  --minimum_update_period 5\
  --mpd_output media/live.mpd

