const log = require("debug")("gstreamer:log");
const error = require("debug")("gstreamer:error");

const Koa = require("koa");
const serve = require("koa-static");
const route = require("koa-route");

const cp = require("child_process");

const fs = require("fs");
const path = require("path");
const util = require("util");

const readFile = util.promisify(fs.readFile);

const app = new Koa();

const startTime = new Date();
const availabilityStartTime = new Date();
const chunks = [];

const child = cp.exec("./live");
child.stdout.on("data", (data) => {
  const str = data.toString("utf-8");
  log(str);
  const matches = str.match(/^([0-9]+)/);
  if (matches) {
    const [, time] = matches;
    const parsedTime = parseInt(time, 10) / 90000;
    if (chunks.length === 0) {
      startTime.setTime(parsedTime * 1000);
      availabilityStartTime.setTime((parsedTime * 1000) + 10000);
    }
    chunks.push(parsedTime);
  }
});
child.stderr.on("data", (data) => {
  const str = data.toString("utf-8");
  error(str);
});
child.on("exit", () => {
  child.kill();
  process.exit(1);
});

app.use(serve("media"));
app.use(route.get("/", async (ctx) => {
  const body = await readFile("index.html", { encoding: "utf-8" });
  ctx.body = body;
}));
app.use(route.get("/live.mpd", (ctx) => {
  const items = Math.floor((Date.now() - startTime.getTime()) / 10000);
  const publishTime = new Date();
  ctx.set("Content-Type", "application/dash+xml");
  ctx.body = `<?xml version="1.0" encoding="UTF-8"?>
<MPD
  xmlns="urn:mpeg:dash:schema:mpd:2011"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xsi:schemaLocation="urn:mpeg:dash:schema:mpd:2011 DASH-MPD.xsd"
  xmlns:cenc="urn:mpeg:cenc:2013"
  profiles="urn:mpeg:dash:profile:isoff-live:2011"
  minBufferTime="PT10S"
  type="dynamic"
  publishTime="${publishTime.toISOString()}"
  availabilityStartTime="${availabilityStartTime.toISOString()}"
  minimumUpdatePeriod="PT5S"
  timeShiftBufferDepth="PT30S"
  suggestedPresentationDelay="PT2S">
  <UTCTiming schemeIdUri="urn:mpeg:dash:utc:http-iso:2014" value="https://time.akamai.com/?iso"/>
  <Period id="0" start="PT0S">
    <AdaptationSet id="0" contentType="video" width="1280" height="800" frameRate="90000/16926" segmentAlignment="true" par="8:5">
      <Representation id="0" bandwidth="876665" codecs="avc1.42c020" mimeType="video/mp4" sar="1:1">
        <SegmentTemplate timescale="90000" media="live_$Time$.m4s">
          <SegmentTimeline>
            <S t="0" d="900000" />
          </SegmentTimeline>
        </SegmentTemplate>
      </Representation>
    </AdaptationSet>
  </Period>
</MPD>`;

}));

app.listen(5000);
